# What's here

Secondo progettino fatto in django, uno scheduler che esegue periodicamente un processo in base alle richieste. [Link di riferimento](https://medium.com/@kevin.michael.horan/scheduling-tasks-in-django-with-the-advanced-python-scheduler-663f17e868e6)

# Setup environment

```
sudo apt install libpq-dev
sudo apt install postgresql-common

conda create --name webscheduler
conda activate web scheduler
pip install apscheduler django psycopg2 requests


django-admin startproject djangoScheduler

python3 manage.py startapp weather
```

# For the db server
[Fonte](https://www.digitalocean.com/community/tutorials/how-to-use-postgresql-with-your-django-application-on-ubuntu-14-04) & [problem solved](https://stackoverflow.com/questions/42653690/psql-could-not-connect-to-server-no-such-file-or-directory-5432-error)
```
sudo apt install postgresql postgresql-contrib libpq-dev
sudo service postgresql start
pg_lsclusters
```

```
CREATE DATABASE myproject;
CREATE USER myprojectuser WITH PASSWORD 'password';
ALTER ROLE myprojectuser SET client_encoding TO 'utf8';
ALTER ROLE myprojectuser SET default_transaction_isolation TO 'read committed';
ALTER ROLE myprojectuser SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE myproject TO myprojectuser;
```
